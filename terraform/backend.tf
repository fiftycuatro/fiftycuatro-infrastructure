# Setup Backend
terraform {
  backend "s3" {
    bucket = "terraform-remote-state.fiftycuatro.com"
    key    = "bastion/main"
    region = "us-east-1"
  }
}
