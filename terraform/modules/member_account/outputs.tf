output "account_id" {
    value = "${var.account_id}"
}

output "group_id" {
    value   = "${aws_iam_group.admins.id}"
}

output "group_name" {
    value = "${aws_iam_group.admins.name}"
}
