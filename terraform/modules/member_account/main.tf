
resource "aws_iam_group" "admins" {
    name = "${var.name}AdminsGroup"
}


resource "aws_iam_group_policy" "admin_policy" {
    name = "${var.name}AdminsPolicy"
    group = "${aws_iam_group.admins.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": "arn:aws:iam::${var.account_id}:role/OrganizationAccountAccessRole"
      },
      {
        "Effect": "Allow",
        "Action": "s3:ListBucket",
       "Resource": "arn:aws:s3:::${var.bucket}"
      },
      {
        "Effect": "Allow",
        "Action": ["s3:GetObject", "s3:PutObject"],
        "Resource": "arn:aws:s3:::${var.bucket}/sites/${var.name}/*"
      }
    ]
}
EOF
}
