# setup aws provider for main account
provider "aws" {
    region = "${var.aws_region}"
}

# For now I do not have any need to limit access developers to a single
# account or not. For now we will just allow all developers have full access
# to all accounts

module "buzzbeaute_staging" {
    source = "./modules/member_account"

    name       = "${var.buzzbeaute_staging["name"]}"
    account_id = "${var.buzzbeaute_staging["account_id"]}"
    bucket     = "${var.remote_state_bucket}"
}

