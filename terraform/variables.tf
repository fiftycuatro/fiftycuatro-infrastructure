variable "remote_state_bucket" {
  default = "terraform-remote-state.fiftycuatro.com"
}

variable "buzzbeaute_staging" {
  type = "map"
}

variable "admin_users" {
  type = "list"
}

variable "aws_region" {
  default = "us-east-1"
}
