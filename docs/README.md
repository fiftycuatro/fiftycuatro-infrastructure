# AWS Infrastructure Documentation

## Setup environment

1. Create and activate a python virtualenv
```
$ virtualenv _env
$ source _env/bin/activate
```
2. Install dependencies
```
$ pip install -r requirements.txt
```

## Building Documentation

1. To build the html version
```
$ make html
```
2. The output should be located in `_build/html`
